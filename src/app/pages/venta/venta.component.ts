import { VentaService } from './../../_service/venta.service';
import { Venta } from './../../_model/venta';
import { MatSnackBar } from '@angular/material';
import { DetalleVenta } from './../../_model/detalleVenta';
import { PersonaService } from './../../_service/persona.service';
import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/_model/persona';
import { ProductoService } from 'src/app/_service/producto.service';
import { Producto } from 'src/app/_model/producto';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {

  personas: Persona[];
  productos: Productos[];
  maxFecha: Date = new Date();
  fechaSeleccionada: Date = new Date();
  cantidad: string;
  importe: string;

  PacienteSeleccionado: Persona;
  detalleVenta_: DetalleVenta[] = [];
  objProducto: Producto;
  mensaje: string;

  constructor(
    private personaService: PersonaService,
    private productoService: ProductoService,
    private snackBar: MatSnackBar,
    private ventaService: VentaService
  ) { }

  ngOnInit() {
    this.listarPersonas();
    this.listarProductos();


  }

  listarPersonas() {
    this.personaService.listar().subscribe(data => {
      this.personas = data;
    });
  }

  listarProductos() {
    this.productoService.listar().subscribe(data => {
      this.productos = data;
    });


  }

  agregar() {
    if (this.cantidad != null) {
      if (this.objProducto != null) {
        let cont = 0;
        for (let i = 0; i < this.detalleVenta_.length; i++) {
          let obj = this.detalleVenta_[i];
          if (obj.producto.idProducto === this.objProducto.idProducto) {
            cont++;
            break;
          }
        }

        if (cont > 0) {
          this.mensaje = 'El producto ya  se encuentra en la lista';
          this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
        } else {

          let det = new DetalleVenta();
          det.cantidad = this.cantidad;
          det.producto = this.objProducto;
          this.detalleVenta_.push(det);


        }
      } else {
        this.mensaje = "Debe de seleccionar un producto";
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      }

    }
  }

  aceptar() {
    let venta = new Venta();
    venta.persona = this.PacienteSeleccionado
    venta.importe = this.importe;
    venta.detalleVenta = this.detalleVenta_;
    //ISODATE
    let tzoffset = (this.fechaSeleccionada).getTimezoneOffset() * 60000;
    let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
    //console.log(localISOTime);//yyyy-mm-ddTHH:mm:ss
    venta.fecha = localISOTime;


    this.ventaService.registrar(venta).subscribe(() => {
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 });
      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });

    // let consultaListaExamenDTO = new ConsultaListaExamenDTO();
    // consultaListaExamenDTO.consulta = consulta;
    // consultaListaExamenDTO.lstExamen = this.examenesSeleccionados;

    // this.consultaService.registrar(consultaListaExamenDTO).subscribe(() => {
    //   this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

    //   setTimeout(() => {
    //     this.limpiarControles();
    //   }, 2000);
    // });
  }
  estadoBotonRegistrar() {
    return (this.detalleVenta_.length === 0);
  }

  limpiarControles() {
    this.detalleVenta_ = [];
    this.PacienteSeleccionado = null;
    this.importe = '';
    this.cantidad = '';
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
  }


  remover(index: number) {
    this.detalleVenta_.splice(index);
  }
}
