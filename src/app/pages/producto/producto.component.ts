import { switchMap } from 'rxjs/operators';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Producto } from 'src/app/_model/producto';
import { ProductoService } from 'src/app/_service/producto.service';
import { ThrowStmt } from '@angular/compiler';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  dataSource: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['idProducto', 'nombre', 'marca', 'acciones'];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private productoService: ProductoService,
    private snack: MatSnackBar,
    public route: ActivatedRoute) {

  }

  ngOnInit() {

    this.productoService.mensajeCambio.subscribe(data => {
      this.snack.open(data, 'AVISO', {
        duration: 2000
      });
    });
    this.productoService.objCambio.subscribe(data => {
      this.onlyData(data);
    });

    this.productoService.listar().subscribe(data => {
      this.onlyData(data);
    });
  }

  onlyData(data: Producto[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  eliminar(data: Producto) {
    this.productoService.eliminar(data.idProducto).pipe(switchMap(() => {
      return this.productoService.listar();
    })).subscribe(data => {
      this.productoService.objCambio.next(data);
      this.productoService.mensajeCambio.next("ELEMENTO ELIMINADO");
    });
  }

  filtrar(valor: String) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

}
