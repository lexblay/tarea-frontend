import { PersonaService } from './../../../_service/persona.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Persona } from 'src/app/_model/persona';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-persona-add',
  templateUrl: './persona-add.component.html',
  styleUrls: ['./persona-add.component.css']
})
export class PersonaAddComponent implements OnInit {

  persona: Persona;

  constructor(
    public dialogRef: MatDialogRef<PersonaAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private personaService: PersonaService) { }

  ngOnInit() {
    this.persona = new Persona();
    this.persona.idPersona = this.data.idPersona;
    this.persona.nombres = this.data.nombres;
    this.persona.apellidos = this.data.apellidos;
  }

  operar() {
    if (this.persona != null && this.persona.idPersona > 0) {
      //BUENA PRACTICA
      this.personaService.modificar(this.persona).pipe(switchMap(() => {
        return this.personaService.listar();
      })).subscribe(data => {
        this.personaService.objCambio.next(data);
        this.personaService.mensajeCambio.next("SE MODIFICO ");
      });
    } else {

      this.personaService.registrar(this.persona).pipe(switchMap(() => {
        return this.personaService.listar();
      })).subscribe(data => {
        this.personaService.objCambio.next(data);
        this.personaService.mensajeCambio.next("SE AGREGO ");
      });

      //  PRACTICA COMUN
      // this.personaService.modificar(this.persona).subscribe(() => {
      //   this.personaService.listar().subscribe(data => {
      //     this.personaService.objCambio.next(data);
      //   });
      // });
    }
    this.onNoClick();
  }



  onNoClick(): void {
    this.dialogRef.close();
  }

}
