import { Persona } from './../../_model/persona';
import { PersonaService } from './../../_service/persona.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { PersonaAddComponent } from './persona-add/persona-add.component';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  dataSource: MatTableDataSource<Persona>;
  displayedColumns: string[] = ['idPersona', 'nombres', 'apellidos', 'acciones'];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private personaService: PersonaService,
    private dialog: MatDialog,
    private snack: MatSnackBar) { }

  ngOnInit() {
    this.personaService.objCambio.subscribe(data => {
      this.onlyData(data);
    });

    this.personaService.mensajeCambio.subscribe(data => {
      this.snack.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.personaService.listar().subscribe(data => {
      this.onlyData(data);
    });
  }

  onlyData(data: Persona[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  openDialog(persona?: Persona) {
    let obj = persona != null ? persona : new Persona();
    this.dialog.open(PersonaAddComponent, {
      width: '400px',
      data: obj
    })
  }

  eliminar(persona: Persona) {
    this.personaService.eliminar(persona.idPersona).pipe(switchMap(() => {
      return this.personaService.listar();
    })).subscribe(data => {
      this.personaService.objCambio.next(data);
      this.personaService.mensajeCambio.next("SE ELIMINO ");
    });
  }

}
