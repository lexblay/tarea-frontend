import { HttpClient } from '@angular/common/http';
import { Venta } from './../_model/venta';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  url: string = `${environment.HOST}/ventas`;

  constructor(private http: HttpClient) { }

  registrar(venta: Venta) {
    return this.http.post(this.url, venta);
  }


}
