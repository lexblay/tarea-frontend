import { Persona } from './../_model/persona';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  objCambio = new Subject<Persona[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/personas`;

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<Persona[]>(this.url);
  }
  modificar(persona: Persona) {
    return this.http.put(this.url, persona);
  }
  registrar(persona: Persona) {
    return this.http.post(this.url, persona);
  }

  eliminar(idPersona: number) {
    return this.http.delete(`${this.url}/${idPersona}`);
  }

}
