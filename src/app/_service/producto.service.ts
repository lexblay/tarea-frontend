import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Producto } from '../_model/producto';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  objCambio = new Subject<Producto[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/productos`;
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<Producto[]>(this.url);
  }

  listarPorId(idProducto: number) {
    return this.http.get<Producto>(`${this.url}/${idProducto}`);
  }

  modificar(producto: Producto) {
    return this.http.put(this.url, producto);
  }
  registrar(producto: Producto) {
    return this.http.post(this.url, producto);
  }

  eliminar(idProducto: number) {
    return this.http.delete(`${this.url}/${idProducto}`);
  }

}
